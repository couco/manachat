import time
import net.mapserv as mapserv
import net.charserv as charserv
import net.stats as stats
import commands
import walkto
import logicmanager
import status
import plugins
import itemdb
# ~ import random
from collections import deque
from net.inventory import get_item_index, get_storage_index
from utils import extends
from actor import find_nearest_being
from chat import send_whisper as whisper

from net.onlineusers import OnlineUsers

__all__ = [ 'PLUGIN', 'init' ]

PLUGIN = {
    'name': 'xcom',
    'requires': ['chatbot'],
    'blocks': (),
}

#FIXME This must be in a library

def preloadArray(nfile):
    try:
        file = open(nfile, "r")
        array=[]
        for x in file.readlines():
            x = x.replace("\n", "")
            x = x.replace("\r", "")
            array.append(x)
        file.close()
        return array
    except:
        print "preloadArray: File " +  nfile + " not found!"

ignored_players     = preloadArray("config/ignored.txt")
disliked_players    = preloadArray("config/disliked.txt")
admins              = preloadArray("config/admins.txt")
friends             = preloadArray("config/friends.txt")

#...until here

XCOMList = preloadArray("config/XCOM.txt")
XCOMServerStatInterested = [] #List of nicks interested in server status change
XCOMBroadcastPrefix = "##B##G "


def online_list_update(curr,prev):
    for x in curr:
        found = False
        for y in prev:
            if x==y: found = True
        if found == False: #detected change
            for nicks in XCOMList: #For every XCOM user...
                if nicks in online_users.online_users: #That's online...
                    if nicks in XCOMServerStatInterested: #If XCOM player is interested
                        if x in XCOMList: #An XCOM user connected?
                            XCOMDelay() #Share its status
                            whisper(nicks, "##W" + x + " is now online [XCOM]")
                        else: #Is a regular server player
                            if x not in XCOMList:
                                XCOMDelay() #Share its status
                                whisper(nicks, "##W" + x + " is now online")

    for x in prev:
        found = False
        for y in curr:
            if x==y: found = True
        if found == False:
            for nicks in XCOMList: #For every XCOM user...
                if nicks in online_users.online_users: #That's online...
                    if nicks in XCOMServerStatInterested: #If XCOM player is interested
                        if x in XCOMList: #An XCOM user connected?
                            XCOMDelay() #Share its status
                            whisper(nicks, "##L" + x + " is now offline [XCOM]")
                        else: #Is a regular server player
                            if x not in XCOMList:
                                XCOMDelay() #Share its status
                                whisper(nicks, "##L" + x + " is now offline")
                                
online_users = OnlineUsers(online_url=' https://server.themanaworld.org/online-old.txt', update_interval=20, refresh_hook=online_list_update)

def XCOMOnlineList(nick, message, is_whisper, match):
    XCOMDelay()
    msg=""
    for nicks in XCOMList:
        if nicks in online_users.online_users:
            msg = msg + nicks + " | "
    XCOMDelay()
    whisper(nick, msg)

def XCOMPrintStat():
    pOnline=0
    xOnline=0
    for p in online_users.online_users:
        pOnline=pOnline+1
        if p in XCOMList:
            xOnline=xOnline+1
    return "%(xOnline)d/%(pOnline)d"%{"pOnline": pOnline, "xOnline": xOnline,}

def XCOMDelay():
    time.sleep(0.1)

def XCOMBroadcast(message):
    for nicks in XCOMList:
        if nicks in online_users.online_users:
            if nicks not in ignored_players:
                XCOMDelay()
                whisper(nicks, message)

def XCOMCommunicate(nick, message, is_whisper, match):
    if not is_whisper:
        return
    if nick in ignored_players:
        return #or say something
    if message[0]=="!":
        return
    if message.startswith("*AFK*:") or message.startswith("WARNING :"): # AFK and WARNING bug workaround
        return
    if nick in XCOMList:
        for nicks in XCOMList:
            if nicks in online_users.online_users:
                if nick==nicks:
                    pass
                else:
                    XCOMDelay()
                    whisper(nicks, "##B##LXCOM[" + XCOMPrintStat() + "]##l " + nick + ": ##b" + message)
    else:
        whisper(nick, XCOMBroadcastPrefix + "XCOM is not enabled (Use !xcon)")

def XCOMSilentInvite(nick, message, is_whisper, match):
    XCOMDelay()
    if not is_whisper:
        return
    if nick in ignored_players:
        return #or say something
    if nick in admins:
        XCOMList.append(match.group(1))
        if match.group(1) not in ignored_players:
            whisper(nick, "##W--- " + nick + " silently invited " + match.group(1) + " on XCOM ---")
        else:
            whisper(nick, "##W" + match.group(1) + " has been ignored by bot and cannot be added to XCOM.")

def XCOMInvite(nick, message, is_whisper, match):
    XCOMDelay()
    if not is_whisper:
        return
    if nick in ignored_players:
        return #or say something
    if nick in admins: # FIXME Do not add if already there!!!
        XCOMList.append(match.group(1))
        XCOMBroadcast("##W--- " + nick + " (Admin) invited " + match.group(1) + " on XCOM ---" + XCOMBroadcastPrefix + match.group(1) + " XCOM enabled! Use !xcoff to disable, use !xclist to see XCOM online list")
    else:
        if nick in ignored_players:
            whisper(nick, "You cannot invite banned players.") 
        else:
            whisper(match.group(1), "##W--- " + nick + " invited you to chat on XCOM --- Answer !xcon to join.")
            XCOMDelay()
            whisper(nick, "Invited " + match.group(1) + " to join XCOM. Waiting for his/her reply...")

XCOMServerInvited = []
def XCOMInviteAll(nick, message, is_whisper, match):
    XCOMDelay()
    if not is_whisper:
        return
    if nick in ignored_players:
        return #or say something
    if nick in admins: # FIXME Do not add if already there!!!
        for invn in online_users.online_users:
            if invn in XCOMList:
                pass
            elif invn in ignored_players:
                pass
            elif invn in XCOMServerInvited:
                pass
            else:
                XCOMServerInvited.append(invn)
                whisper(invn, "##W--- " + nick + " invited you to chat on XCOM --- Answer !xcon to join.")
                XCOMDelay()


def XCOMEnable(nick, message, is_whisper, match):
    XCOMDelay()
    #accept only whispers
    if not is_whisper:
        return
    if nick in ignored_players:
        return #or say something
    #search array
    if nick in XCOMList:
        whisper(nick, XCOMBroadcastPrefix + nick + " XCOM already enabled")
    else:
        XCOMList.append(nick)
        XCOMBroadcast("##W--- " + nick + " is online on XCOM ---" + XCOMBroadcastPrefix + nick + " XCOM enabled! Use !xcoff or !xcom off to disable, use !xclist to see XCOM online list")

def XCOMDisable(nick, message, is_whisper, match):
    XCOMDelay()
    #accept only whispers
    if not is_whisper:
        return
    if nick in ignored_players:
        return #or say something
    #search array
    if nick in XCOMList:
        XCOMBroadcast("##L--- " + nick + " disabled XCOM ---")
        XCOMList.remove(nick)
    else:
        whisper(nick, XCOMBroadcastPrefix + nick + " XCOM already disabled")

def XCOMServerInterestEnable(nick, message, is_whisper, match):
    XCOMDelay()
    #accept only whispers
    if not is_whisper:
        return
    if nick in ignored_players:
        return #or say something
    #search array
    if nick in XCOMList:
        whisper(nick, XCOMBroadcastPrefix + "Server online status notifications enabled!")
        XCOMServerStatInterested.append(nick)

def XCOMServerInterestDisable(nick, message, is_whisper, match):
    XCOMDelay()
    #accept only whispers
    if not is_whisper:
        return
    if nick in ignored_players:
        return #or say something
    #search array
    if nick in XCOMList:
        whisper(nick, XCOMBroadcastPrefix + "Server online status notifications disabled!")
        XCOMServerStatInterested.remove(nick)

def XCOMBan(nick, message, is_whisper, match):
    XCOMDelay()
    #accept only whispers
    if not is_whisper:
        return
    if nick in admins:
        #search array
        if match.group(1) in ignored_players:
            whisper(nick, "Already banned.")
        else:
            ignored_players.append(match.group(1))
            XCOMList.remove(match.group(1))
            #FIXME array need to be saved!!!
            XCOMBroadcast(XCOMBroadcastPrefix + match.group(1) + " is now banned from XCOM")
    else:
        whisper(nick, "Admins only.")

def XCOMUnBan(nick, message, is_whisper, match):
    XCOMDelay()
    #accept only whispers
    if not is_whisper:
        return
    if nick in admins:
        #search array
        if match.group(1) in ignored_players:
            XCOMList.append(match.group(1))
            ignored_players.remove(match.group(1))
            #FIXME array need to be saved!!!
            XCOMBroadcast(XCOMBroadcastPrefix + match.group(1) + " is now unbanned from XCOM")
            whisper(match.group(1), "You are now unbanned from XCOM. Don't make it happen again.")
        else:
            whisper(nick, "Already banned.")
    else:
        whisper(nick, "Admins only.")

# =============================================

def cmd_commands(nick, message, is_whisper, match):
    if not is_whisper:
        return
    if nick in ignored_players:
        return

    c = []
    for cmd in xcom_commands:
        if cmd.startswith('!('):
            br = cmd.index(')')
            c.extend(cmd[2:br].split('|'))
        elif cmd.startswith('!'):
            c.append(cmd[1:].split()[0])

    c.sort()
    whisper(nick, ', '.join(c))

xcom_commands = {
    '!xcomhelp' : cmd_commands,
    '!xcon' : XCOMEnable,
    '!xcom' : XCOMEnable,
    '!xcoff' : XCOMDisable,
    '!xcom off' : XCOMDisable,
    '!xclist' : XCOMOnlineList,
    '!xci (.*)' : XCOMInvite,
    '!xcia' : XCOMInviteAll,
    '!xcsi (.*)' : XCOMSilentInvite,
    '!xcb (.*)' : XCOMBan,
    '!xcu (.*)' : XCOMUnBan,
    '!xcsion' : XCOMServerInterestEnable,
    '!xcsioff' : XCOMServerInterestDisable,
    r'(.*)' : XCOMCommunicate,
}

# ~ def chatbot_answer_mod(func):
    # ~ '''modifies chatbot.answer to remember last 10 commands'''

    # ~ def mb_answer(nick, message, is_whisper):
        # ~ return func(nick, message, is_whisper)

    # ~ return mb_answer

def init(config):

    for cmd, action in xcom_commands.items():
        plugins.chatbot.add_command(cmd, action)
    online_users.start()
    # ~ plugins.chatbot.answer = chatbot_answer_mod(plugins.chatbot.answer)
    
